package br.com.dbccompany.hackathon.leitorDados.Config;

import br.com.dbccompany.hackathon.leitorDados.Entity.VendedorEntity;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@Configuration
@EnableBatchProcessing
public class SpringBatchConfig {

    // ver se é melhor usar DTO ou ENTITY
    @Autowired
    public JobBuilderFactory jobBuilderFactory;
    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job job(JobBuilderFactory jobBuilderFactory,
                   StepBuilderFactory stepBuilderFactory,
                   ItemReader<VendedorEntity> itemReader,
                   ItemWriter<VendedorEntity> itemWriter){

        Step step = stepBuilderFactory.get("ETL-file-load")
                .<VendedorEntity,VendedorEntity>chunk(10)
                .reader(itemReader)
                .writer(itemWriter)
                .build();

        return jobBuilderFactory.get("ETL-Load")
                .incrementer(new RunIdIncrementer())
                .start(step)
                .build();
    }

    @Bean
    public FlatFileItemReader<VendedorEntity> itemReader(@Value("${file: /vendedor.dat}")Resource resource) {
        FlatFileItemReader<VendedorEntity> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setResource(new ClassPathResource("vendedor.dat"));
        flatFileItemReader.setName("DAT-Reader");
        flatFileItemReader.setLineMapper(linerMapper());

        System.out.println("////////////////////////////////////////");
        System.out.println("flatFileItemReader: " + flatFileItemReader);
        System.out.println("////////////////////////////////////////");


        return flatFileItemReader;
    }

    @Bean
    public LineMapper<VendedorEntity> linerMapper() {

        DefaultLineMapper<VendedorEntity> defaultLineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter("ç");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames(new String[]{"id","cpf","name","salary"});// config para vendedor
        // verificar como ficaria para cada tipo
        BeanWrapperFieldSetMapper<VendedorEntity> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(VendedorEntity.class);

        defaultLineMapper.setLineTokenizer(lineTokenizer);
        defaultLineMapper.setFieldSetMapper(fieldSetMapper);

        return defaultLineMapper;
    }
}
