package br.com.dbccompany.hackathon.leitorDados.DTO;

import br.com.dbccompany.hackathon.leitorDados.Entity.VendedorEntity;

public class VendedorDTO {

    private String id;
    private String cpf;
    private String nome;
    private String salary;

    public void VendedorDTO(){}

    public VendedorDTO(VendedorEntity vendedor) {
        this.id = vendedor.getId();
        this.cpf = vendedor.getCpf();
        this.nome = vendedor.getNome();
        this.salary = vendedor.getSalary();
    }

    public VendedorEntity convert(){
        VendedorEntity vendedor = new VendedorEntity();
        vendedor.setId( this.id );
        vendedor.setCpf( this.cpf );
        vendedor.setNome( this.nome );
        vendedor.setSalary( this.salary );

        return vendedor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }
}
