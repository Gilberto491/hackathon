package br.com.dbccompany.hackathon.leitorDados.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ClienteEntity {

    @Id
    private String id;
    private String cnpj;
    private String nome;
    private String business;

    public ClienteEntity() {}

    public ClienteEntity(String id, String cnpj, String nome, String business) {
        this.id = id;
        this.cnpj = cnpj;
        this.nome = nome;
        this.business = business;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }
}
