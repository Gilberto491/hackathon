package br.com.dbccompany.hackathon.leitorDados.DTO;

import br.com.dbccompany.hackathon.leitorDados.Entity.ItemEntity;
import br.com.dbccompany.hackathon.leitorDados.Entity.VendaEntity;

public class ItemDTO {

    private int id;
    private int quantidade;
    private double preco;
    private VendaEntity venda;

    public ItemDTO() {}

    public ItemDTO(ItemEntity item) {
        this.id = item.getId();
        this.quantidade = item.getQuantidade();
        this.preco = item.getPreco();
        this.venda = item.getVenda();
    }

    public ItemEntity convert(){
        ItemEntity item = new ItemEntity();
        item.setId(this.id);
        item.setQuantidade(this.quantidade);
        item.setPreco(this.preco);
        item.setVenda(this.venda);

        return item;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public VendaEntity getVenda() {
        return venda;
    }

    public void setVenda(VendaEntity venda) {
        this.venda = venda;
    }
}
