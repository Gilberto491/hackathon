package br.com.dbccompany.hackathon.leitorDados.Batch;

import br.com.dbccompany.hackathon.leitorDados.Entity.VendedorEntity;
import br.com.dbccompany.hackathon.leitorDados.Repository.VendedorRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DBWriter implements ItemWriter<VendedorEntity> {

    @Autowired
    private VendedorRepository repository;

    @Override
    public void write(List<? extends VendedorEntity> vendedores) throws Exception {
        List<VendedorEntity> newVendedores = new ArrayList<>();

        for(VendedorEntity vendedor : vendedores){
            if(vendedor.getId().equals("001")){
                newVendedores.add(vendedor);
            }
            System.out.println("\nVendedor: " + vendedor.getNome());
        }

        repository.saveAll(newVendedores);

    }
}
