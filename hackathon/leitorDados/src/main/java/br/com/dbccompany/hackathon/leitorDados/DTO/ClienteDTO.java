package br.com.dbccompany.hackathon.leitorDados.DTO;

import br.com.dbccompany.hackathon.leitorDados.Entity.ClienteEntity;

public class ClienteDTO {

    private String id;
    private String cnpj;
    private String nome;
    private String business;

    public ClienteDTO(){}

    public ClienteDTO(ClienteEntity cliente) {
        this.id = cliente.getId();
        this.cnpj = cliente.getCnpj();
        this.business = cliente.getBusiness();
        this.nome = cliente.getNome();
    }

    public ClienteEntity convert(){
        ClienteEntity cliente = new ClienteEntity();
        cliente.setId( this.id );
        cliente.setCnpj( this.cnpj );
        cliente.setBusiness( this.business );
        cliente.setNome( this.nome );

        return cliente;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }
}
