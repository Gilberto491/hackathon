package br.com.dbccompany.hackathon.leitorDados.Repository;

import br.com.dbccompany.hackathon.leitorDados.Entity.VendedorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VendedorRepository extends JpaRepository<VendedorEntity, String> {
}
