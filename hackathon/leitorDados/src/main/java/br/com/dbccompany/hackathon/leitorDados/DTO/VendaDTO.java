package br.com.dbccompany.hackathon.leitorDados.DTO;

import br.com.dbccompany.hackathon.leitorDados.Entity.ItemEntity;
import br.com.dbccompany.hackathon.leitorDados.Entity.VendaEntity;

import java.util.List;

public class VendaDTO {

    private String id;
    private String nome;
    private List<ItemEntity> itens;

    public VendaDTO() {}

    public VendaDTO(VendaEntity venda) {
        this.id = venda.getId();
        this.nome = venda.getNome();
        this.itens = venda.getItens();
    }

    public VendaEntity convert(){
        VendaEntity venda = new VendaEntity();

        venda.setId(this.id);
        venda.setNome(this.nome);
        venda.setItens(this.itens);

        return venda;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ItemEntity> getItens() {
        return itens;
    }

    public void setItens(List<ItemEntity> itens) {
        this.itens = itens;
    }
}
