package br.com.dbccompany.hackathon.leitorDados.Repository;

import br.com.dbccompany.hackathon.leitorDados.Entity.VendaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VendaRepository extends JpaRepository<VendaEntity, String> {}
