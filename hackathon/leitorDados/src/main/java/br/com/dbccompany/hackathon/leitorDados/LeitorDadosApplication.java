package br.com.dbccompany.hackathon.leitorDados;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeitorDadosApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeitorDadosApplication.class, args);
	}

}
