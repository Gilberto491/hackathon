package br.com.dbccompany.hackathon.leitorDados.Repository;

import br.com.dbccompany.hackathon.leitorDados.Entity.ClienteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<ClienteEntity, String> {}
