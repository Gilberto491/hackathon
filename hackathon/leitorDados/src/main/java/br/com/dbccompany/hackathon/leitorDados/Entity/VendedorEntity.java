package br.com.dbccompany.hackathon.leitorDados.Entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class VendedorEntity {

    @Id
    private String id;
    private String cpf;
    private String nome;
    private String salary;

    public VendedorEntity() {}

    public VendedorEntity(String id, String cpf, String nome, String salary) {
        this.id = id;
        this.cpf = cpf;
        this.nome = nome;
        this.salary = salary;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "VendedorEntity{" +
                "id='" + id + '\'' +
                ", cpf='" + cpf + '\'' +
                ", nome='" + nome + '\'' +
                ", salary=" + salary +
                '}';
    }
}
