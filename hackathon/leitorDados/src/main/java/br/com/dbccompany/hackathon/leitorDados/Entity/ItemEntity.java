package br.com.dbccompany.hackathon.leitorDados.Entity;

import javax.persistence.*;

@Entity
public class ItemEntity {

    @Id
    private int id;
    private int quantidade;
    private double preco;

    public ItemEntity() {}

    public ItemEntity(int id, int quantidade, double preco) {
        this.id = id;
        this.quantidade = quantidade;
        this.preco = preco;
    }

    @ManyToOne
    private VendaEntity venda;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public VendaEntity getVenda() {
        return venda;
    }

    public void setVenda(VendaEntity venda) {
        this.venda = venda;
    }
}
