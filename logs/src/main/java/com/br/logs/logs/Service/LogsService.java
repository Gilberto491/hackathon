package com.br.logs.logs.Service;

import com.br.logs.logs.DTO.LogsDTO;
import com.br.logs.logs.Entity.LogsEntity;
import com.br.logs.logs.Repository.LogsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogsService {

    @Autowired
    private LogsRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public LogsEntity salvar(LogsEntity entidade){
        return repository.save(entidade);
    }

    public List<LogsEntity> todos() {
        return (List<LogsEntity>) repository.findAll();
    }

    public LogsEntity porId( Integer id) {
        return repository.findById(id).get();
    }

    public List<LogsDTO> retornarListaLogs(){
        List<LogsDTO> listaDTO = new ArrayList<>();
        for(LogsEntity logs: this.todos()){
            listaDTO.add(new LogsDTO(logs));
        }
        return listaDTO;
    }

    public LogsEntity salvarLogs(LogsDTO dto){
        LogsEntity logs = this.salvar(dto.convert());
        return logs;
    }

    public LogsDTO retonarLogsEspecificos(Integer id){
        LogsDTO logs = new LogsDTO(this.porId(id));
        return logs;
    }


}
