package com.br.logs.logs.Repository;

import com.br.logs.logs.Entity.LogsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogsRepository extends CrudRepository<LogsEntity, Integer> {

     LogsEntity findByCodigo(String codigo);
     LogsEntity findByMensagem(String mensagem);
}
