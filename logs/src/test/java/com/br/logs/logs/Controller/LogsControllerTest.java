package com.br.logs.logs.Controller;

import com.br.logs.logs.Entity.LogsEntity;
import com.br.logs.logs.Repository.LogsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class LogsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private LogsRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoLogs() throws Exception {
        URI uri = new URI("/api/logs/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmLog() throws Exception {

        URI uri = new URI("/api/logs/novo");
        String json = "{\"mensagem\": \"teste\", \"codigo\": \"teste2\" }";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.mensagem")
                .value("teste")
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.codigo")
                .value("teste2")
        );
    }

    @Test
    public void deveRetornarUmLog() throws Exception {
        LogsEntity logs = new LogsEntity();
        logs.setCodigo("teste");
        logs.setMensagem("teste");

        LogsEntity newLogs = repository.save(logs);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/logs/ver/{id}", newLogs.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.codigo")
                .value("teste")
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.mensagem")
                .value("teste")
        );
    }
}
