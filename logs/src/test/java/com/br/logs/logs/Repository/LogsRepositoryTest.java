package com.br.logs.logs.Repository;

import com.br.logs.logs.Entity.LogsEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class LogsRepositoryTest {

    @Autowired
    private LogsRepository repository;

    @Test
    public void salvarCodigoMensagem(){
        LogsEntity logs = new LogsEntity();
        logs.setMensagem("teste");
        logs.setCodigo("teste123");
        repository.save(logs);
        assertEquals(logs.getMensagem(), repository.findByMensagem("teste").getMensagem());
        assertEquals(logs.getCodigo(), repository.findByCodigo("teste123").getCodigo());
    }

    @Test
    public void buscarCodigoMensagem(){
        String mensagem = "teste";
        String codigo = "teste123";
        assertNull(repository.findByMensagem(mensagem));
        assertNull(repository.findByCodigo(codigo));
    }
}
