package br.com.hackaton.api.Repository;

import br.com.hackaton.api.Entity.Enum.TipoUsuarioEnum;
import br.com.hackaton.api.Entity.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository repository;

    @Test
    public void salvarLoginSenhaTipoUsuario(){
        UserEntity user = new UserEntity();
        user.setLogin("teste");
        user.setSenha("teste");
        user.setTipoUsuario(TipoUsuarioEnum.ANALISTA);
        repository.save(user);

        assertEquals(user.getLogin(), repository.findByLogin("teste").getLogin());
        assertEquals(user.getSenha(), repository.findBySenha("teste").getSenha());
        assertEquals(user.getTipoUsuario(), repository.findByTipoUsuario(TipoUsuarioEnum.ANALISTA).getTipoUsuario());
    }

    @Test
    public void buscarLoginSenhaTipoUsuario(){
        String login = "teste";
        String senha = "teste";
        TipoUsuarioEnum tipoUsuario = TipoUsuarioEnum.ANALISTA;
        assertNull(repository.findByLogin("teste"));
        assertNull(repository.findBySenha("teste"));
        assertNull(repository.findByTipoUsuario(TipoUsuarioEnum.ANALISTA));
    }
}
