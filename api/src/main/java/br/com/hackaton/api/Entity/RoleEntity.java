package br.com.hackaton.api.Entity;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.List;

@Entity
public class RoleEntity implements GrantedAuthority {

    @Id
    private String nomeRole;

    @ManyToMany
    @JoinTable(name = "users_roles",
            joinColumns = { @JoinColumn( name = "role_id")},
            inverseJoinColumns = { @JoinColumn( name = "user_id")}
    )
    private List<UserEntity> users;

    public String getNomeRole() {
        return nomeRole;
    }

    public void setNomeRole(String nomeRole) {
        this.nomeRole = nomeRole;
    }

    @Override
    public String getAuthority() {
        return this.nomeRole;
    }

    public List<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }
}
