package br.com.hackaton.api.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/user")
public class UserController {

    /*
        Implementar metodos para
        chamar os dados e para consultar
        todos e algum especifico
    */

    @GetMapping(value = "/chamar")
    @ResponseBody
    public String chamadaDados(){
        return "teste";
    }

    @GetMapping(value = "/consultar")
    @ResponseBody
    public String consultarDados(){
        return "teste";
    }



}
