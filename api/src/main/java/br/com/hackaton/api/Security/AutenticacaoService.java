package br.com.hackaton.api.Security;

import br.com.hackaton.api.Entity.UserEntity;
import br.com.hackaton.api.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AutenticacaoService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        UserEntity users = userRepository.findByLogin(login);
        if(users == null){
            throw new UsernameNotFoundException("User nulo");
        }
        return new User(users.getUsername(), users.getPassword(), true,true,true,true, users.getAuthorities());
    }
}
